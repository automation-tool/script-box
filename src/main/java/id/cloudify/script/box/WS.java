package id.cloudify.script.box;

import java.io.File;

public class WS {

	File location;
	
	public File set(File location) {
		this.location = location;
		return location;
	}
	
	public File set(String location) {
		return set(new File(location));
	}

	public File up() {
		return set(getLocation().getParentFile());
	}
	
	public File getLocation() {
		return location;
	}
	
}

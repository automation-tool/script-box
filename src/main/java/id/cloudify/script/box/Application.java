package id.cloudify.script.box;

import java.io.File;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.HandlerMapping;

import com.google.gson.GsonBuilder;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;

@SpringBootApplication
@RestController
public class Application {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	File workdir;
	File trigger;
	File script;
	File workspace;
	File outcome;
	File schedule;
	File variable;
	
	GsonBuilder gsonBuilder;
	
	@PostConstruct
	public void init() {
		
		workdir = new File(System.getenv("WORKDIR"));
		if (!workdir.exists()) workdir.mkdirs();
		
		trigger = new File(workdir, "trigger");
		if (!trigger.exists()) trigger.mkdirs();
		
		script = new File(workdir, "script");
		if (!script.exists()) script.mkdirs();
		
		workspace = new File(workdir, "workspace");
		if (!workspace.exists()) workspace.mkdirs();
		
		outcome = new File(workdir, "outcome");
		if (!outcome.exists()) outcome.mkdirs();
		
		schedule = new File(workdir, "schedule");
		if (!schedule.exists()) schedule.mkdirs();
		
		variable = new File(workdir, "variable");
		if (!variable.exists()) variable.mkdirs();
		
		
		gsonBuilder = new GsonBuilder().setPrettyPrinting();
		
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/run/**", method=RequestMethod.POST)
	public void accept(@RequestBody Map payload, HttpServletRequest request) throws Exception {
		
		String path = ((String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE)).substring(5);
		
		String sessionId = path.concat("-").concat(Long.toString(System.nanoTime(), 36));
		File json = new File(trigger, sessionId.concat(".json"));
		logger.info(sessionId);
		
		FileUtils.writeStringToFile(json, gsonBuilder.create().toJson(payload));
		
		executorService.execute(new Runnable() {
			
			@Override
			public void run() {
				try { execute(sessionId, path.concat(".txt"), payload); } catch (Exception e) {
					logger.error(e.getMessage());
				}
			}
			
		});
		
		
	}
	
	ExecutorService executorService = Executors.newCachedThreadPool();
	
	@SuppressWarnings("rawtypes")
	public void execute(String sessionId, String script, Map args) throws Exception {
		
		Binding binding = new Binding();
		
		Logger log = LoggerFactory.getLogger(sessionId);
		WS ws = new WS();
		
		ws.set(new File(workspace, sessionId)).mkdirs();
		
		binding.setVariable("args", args);
		binding.setVariable("WS", ws);
		binding.setVariable("CLi", new CLi(log, ws));
		binding.setVariable("JSON", new JSON());
		binding.setVariable("http", new Http());
		binding.setVariable("log", log);
		
		GroovyShell shell = new GroovyShell(binding);
		
		Object value = shell.evaluate(FileUtils.readFileToString(new File(this.script, script)));
		FileUtils.writeStringToFile( new File(outcome, sessionId.concat(".json")), gsonBuilder.create().toJson(value));
		
	}

}

 	

package id.cloudify.script.box;

import java.util.Map;

import com.google.gson.GsonBuilder;

public class JSON {
	
	GsonBuilder gsonBuilder = new GsonBuilder().setPrettyPrinting();
	
	public Map parse(String text) {
		return gsonBuilder.create().fromJson(text, Map.class);
	}
	
	public String stringify(Object data) {
		return gsonBuilder.create().toJson(data);
	}
	
}

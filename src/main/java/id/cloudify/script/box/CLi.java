package id.cloudify.script.box;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

import org.slf4j.Logger;
import org.springframework.cglib.proxy.UndeclaredThrowableException;

public class CLi {

	Logger log;
	WS ws;
	
	public CLi(Logger log, WS ws) {
		this.log = log;
		this.ws = ws;
	}

	public Execution exec(Map args) throws Exception {
		return new Execution(args);
	}

	public class Execution {

		String stdOut;
		int exitValue;

		public Execution(Map args) throws Exception {

			String cmd = args.get("cmd").toString();
			
			final Process p = Runtime.getRuntime().exec(cmd, new String[0], ws.getLocation());
			log.info(cmd);
			
			final StringBuilder stdOut = new StringBuilder();

			new Thread(new Runnable() {

				@Override
				public void run() {

					BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
					String line = null;

					try {
						
						while ((line = input.readLine()) != null) {
							log.info(line);
							stdOut.append(line);
							stdOut.append(System.lineSeparator());
						}
						
					} catch (IOException e) {
						throw new UndeclaredThrowableException(e);
					}


				}

			}).start();
			
			final StringBuilder errOut = new StringBuilder();
			
			new Thread(new Runnable() {

				@Override
				public void run() {

					BufferedReader input = new BufferedReader(new InputStreamReader(p.getErrorStream()));
					String line = null;

					try {

						while ((line = input.readLine()) != null) {
							errOut.append(line);
							errOut.append(System.lineSeparator());
						} 

					} catch (IOException e) {
						throw new UndeclaredThrowableException(e);
					}

				}

			}).start();

			
			while (p.isAlive()) { Thread.sleep(10); }
			this.exitValue = p.waitFor();
			this.stdOut = stdOut.toString().trim();
			
			if (this.exitValue != 0) log.error(errOut.toString().trim());
			
		}

		public String getStdOut() {
			return stdOut;
		}

		public int getExitValue() {
			return exitValue;
		}

	}

}
